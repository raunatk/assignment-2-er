import ballerina/log;
import ballerina/http;
import ballerinax/kafka;
import ballerinax/docker;
import ballerinax/mongodb;

public type GloceriesCompany record {|
    string companyId;
    Store store;
|};

public type Store record {|
    string storeId;
    Glocery item;
|};

public type Glocery record {|
    string itemName;
    int quantity;
    decimal price;
|};

public type Order record {|
    int orderId;
    string storeId;
    string itemName;
    int quantity;
    decimal price;
    boolean isValid;
|};

public type Delivery record {|
    string address;
|};


// Create a subtype of `kafka:AnydataProducerRecord`.
public type OrderProducerRecord record {|
    *kafka:AnydataProducerRecord;
    GloceriesCompany companyName;
    Order value;
    Delivery destination;
    int key;
|};

// set up kafka configuration
kafka:ProducerConfiguration producerConfigs = {
    clientId: "basic_producer",
    acks: "all",
    retryCount: 3,
    enableIdempotence: true,
    orderId: "test-ordering-id"
};

// set up mongoDB configurable
mongodb:ClientConfig mongoConfig = {

        host: "localhost",
        port: 27017,
        username: "admin1",
        password: "admin1",
        options: {sslEnabled: false, serverSelectionTimeout: 5000}
};

string database = "myDatabase"
mongodb:Client mongoClient = checkpanic new (mongoConfig, "Order");

kafka:Producer orderProducer = check new (kafka:DEFAULT_URL);

public function main() returns error? {
    OrderProducerRecord producerRecord = {
        key: 1,
        topic: "kafka-first-topic",
        companyName: {
            companyId: "otbC",
            store: {
                storeId: "otb",
                item: {
                    itemName: "bath creame",
                    quantity: 5,
                    price: 10
                }
            }
        },
        value: {
            orderId: 1,
            storeId: "otb",
            quantity: 5,
            itemName: "bath creame",
            price: 10,
            isValid: true
        },
        destination: {
            address: "private bag 784"
        }
    };
    check orderProducer->send(producerRecord);

    string dataStore = "data_base";

    map<json> GloceriesCompany = { companyName: {
            companyId: "otb",
            store: {
                storeId: "otb",
                item: {
                    itemName: "bath creame",
                    quantity: 7,
                    price: 7.20
                }
            }
    }
    };
    map<json> orderReq = { "orderId": "o1", "storeId": "otb", "itemName": "bath creame", "quantity": "7", "price": 7.20 };
    map<json> deliveryInfo = { "address": "private bag 784" };

    log:printInfo("Ordering Gloceries:");
    checkpanic mongoClient->insert(GloceriesCompany,"dataStore");
    checkpanic mongoClient->insert(orderReq,"dataStore");
    checkpanic mongoClient->insert(deliveryInfo,"dataStore");
  
    log:printInfo("Derivering Order: ");
    map<json>[] jsonRet = checkpanic mongoClient->find("dataStore",(),());
    log:printInfo("Returned documents '" + jsonRet.toString() + "'.");
    map<json> queryString = {"address": "private bag 2415" };
    jsonRet = checkpanic mongoClient->find("dataStore", (), queryString);
    log:printInfo("Returned Filtered documents '" + jsonRet.toString() + "'.");

    log:printInfo("Updating Order:");
    map<json> replaceFilter = { "storeId": "PEP505" };
    map<json> replaceDoc = { "itemName": "creme oil", "quantity": "8" };
    int response = checkpanic mongoClient->update(replaceDoc,"dataStore", (), replaceFilter, true);
    if (response > 0 ) {
        log:printInfo("Modified order: '" + response.toString() + "'.") ;
    } else {
        log:printInfo("Error in replacing data");
    }
   log:printInfo("Deleting Order:");
   map<json> deleteFilter = { "orderId": "o1" };
   int deleteRet = checkpanic mongoClient->delete("dataStore", (), deleteFilter, true);
   if (deleteRet > 0 ) {
       log:printInfo("Delete order: '" + deleteRet.toString() + "'.") ;
   } else {
       log:printInfo("Error in deleting data");
   }
     mongoClient->close();
}