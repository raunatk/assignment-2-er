import ballerina/io;
import ballerina/log;
import ballerina/kafka;
import ballerina/http;
import ballerinax/docker;
import ballerinax/mongodb;

kafka:ConsumerConfig consumerConfig = {
    bootstrapServers: "localhost:9091, localhost:9092",
    groupId: "order-deliver",
    topics: ["item-details"],
    pollingIntervalInMillis: 1000,
    autoCommit:false
};

// set up mongo Database
mongodb:ClientConfig mongoConfig = {

        host: "localhost",
        port: 2727,
        username: "Admin",
        password: "Admin",
        options: {sslEnabled: false, serverSelectionTimeout: 5000}
};

public type GloceriesCompany record {|
    string companyId;
    Store store;
|};

public type Store record {|
    string storeId;
    Glocery item;
|};

public type Glocery record {|
    string itemName;
    int quantity;
    decimal price;
|};

public type Order record {|
    int orderId;
    string storeId;
    string itemName;
    int quantity;
    decimal price;
    boolean isValid;
|};

public type Deliver record {|
    string address;
|};

public type OrderConsumerRecord record {|
    *kafka:AnydataConsumerRecord;
    GloceriesCompany companyName;
    Order value;
    Deliver destination;
|};

listener http:Listener itemOrderEP = new(9090);

@http:ServiceConfig {
    basePath:"/glocery-order"
}

service on new kafka:Listener(kafka:DEFAULT_URL, consumerConfigs) {
    
    remote function orderItemRecord(OrderConsumerRecord[] records) returns error? {

        check from OrderConsumerRecord orderRecord in records
            where orderRecord.value.isValid
            do {
                log:printInfo("Received Valid Order: " + orderRecord.value.toString());
            };
    }
    
    remote function deliveyRecord(OrderConsumerRecord[] records) returns error? {

        check from OrderConsumerRecord orderRecord in records
            where deliverRecord.destination.address
            do {
                log:printInfo("Delivered Valid Order: " + deliverRecord.destination.toString());
            };
    }

    remote function updateOrder(OrderConsumerRecord[] records) returns error? {
       
        check from OrderConsumerRecord orderRecord in records
            where updateRecord.value.isValid
            do {
                log:printInfo("Updated Order: " + updateRecord.value.toString());
            };
    }

    remote function cancelOrder(OrderConsumerRecord[] records) returns error? {
        
        check from OrderConsumerRecord orderRecord in records
            where cancelRecord.value.isValid
            do {
                log:printInfo("Cancel Order: " + cancelRecord.value.toString());
            };
    }
}
